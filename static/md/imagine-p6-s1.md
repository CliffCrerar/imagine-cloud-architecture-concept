# Cloud Information Media

---

> This page was just added as a nice to have, AWS have really short and interesting videos that describe their services.

## AWS Cloud Infra structure

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/RPis5mbM8c8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## High Preformance Computing

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/evpE9T3nXxw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Single Sign On

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/_qNkFxp1Z_k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Elastisearch

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/4Zw1IOxW-oA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Private Link

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/bRshzwK9ApI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## EMR

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/QuwaBOESGiU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Aurora

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/IwyOyGs69N4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Monitoring

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/aU6A-45c7Vs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Compliance

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/U632-ND7dKQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Building Applications

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/Mr0ZOnjwsXk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Pricing

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/op_9NfAVedY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Organisations

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/T4NK8fv8YdI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Workspaces

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/TGXQrk4dmI8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Backups

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/QDiXzFx2iMU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Databases

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/eMzCI7S1P9M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Cloud watch

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/a4dhoTQCyRA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Work docs

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/DGQGYgiYdnA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Media delivery

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/IRG7ThJ4mNk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Quantum Ledger Database

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/jcZ_rsLJrqk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Machine Learning Powering Business

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/ovb6sqYwYs8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>