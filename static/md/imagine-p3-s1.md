# Business infrastructure 

---

Although a fully cloud based solution is well within the realms of possibility there is still a a degree of physical hardware required, minimally productivity devices (laptops or desktops) and a connection to the internet. Decisions, for on premise infrastructure and infrastructure as a service (IaaS) will be based on access, nature of operations, place of business and the services included in the lease agreement of office space.

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/evpE9T3nXxw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
---

> [Cloud Info Media](/info-media) is a "mixed tape" for my favorite AWS videos, I added it as a nice to have. The relevant videos are referenced and will a icon "insert icon" if the link references that page.

