# Burden of Management {class="align-left"}

Solutions Architect is my vocation, to me it is a responsibility more than a title or a cheque in my bank account. That responsibility is to not only be able, but enabled to solve any problem that I could possibly be faced with, and in my opinion there is but one way to do this. To ensure that I am constantly in a humble state of learning everyday, learning from everyone, junior and senior, chairman and cleaner.

---

<div>                
<img class="img-fluid" src="https://cdn-cloudflare.ga/assets/flat-img-gif/1544094121_web-maint-1.jpg" alt="the setup"/>
</div>

---


## Matter of strategy {class="align-left"}

---

My point of departure will be to gain a comprehension of:

{class="ul"}
- the main drivers of revenue.
- the vision and strategic objectives of the company and its directors.
- who the current / potential customers are, target market and market share.
- the environment the company will operate in external and internal.
- the existing third party tech / data.
- what are the key performance indicators that would determine the success. (Obviously the bottom line, but what else)\

---

<div>
<img class="img-fluid" src="static/img/rocket-space-flat-3d.gif" alt="rocket light speed">
</div>

---

> I will mention AWS when referring to cloud services. However; these services are cloud provider agnostic.

## Determine objectives {class="align-left"}

---

Following this, after the direction and values are well understood, considering the position of the business I will then write a mission statement. From it, I will derive a strategic objective, or objectives as an executive. Subsequently tactical objectives will be derived for daily / weekly tasks. It will serve as milestones and indicators of the success, while inline with management's expectation. Determinants of the formulation hereof will also depend on the frequency of management's performance reviews.

> **Note:** what I describe here might not be immediately relevant since the company is a startup but will be an inevitability as the company grows.

These objectives, depending on the delegation of authority, will encapsulate:

- Business infrastructure.
- Operational Readiness and Technology policies.
- Software engineering and development operations.
- The role of technology and social media related to the marketing strategy.
- Support to customers / employees.
- Disaster recovery.

Each one of the above will require exploration of the known and unknown to determine optimial execution.

---

<div>
<img class="img-fluid" src="static/img/up-in-space.gif" alt="up in space">
</div>

