# Introduction

#### Before I developed software

I was a consultant once, for a long time, before I became a student of tech. I used be articulate, these days not so much anymore. I find I express myself better in writing than in speech. Recent years my thoughts translated into features of an application. Before I presented it in a boardrooms. Only a bump in the road, exposure to the right environment will swiftly resolve this. In contrast, I have always been a man of action, I am not a good talker. What I can do is what will impress, not what I can say, but what I can do.