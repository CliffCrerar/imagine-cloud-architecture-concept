## Motivation {class="align-left"}

---
To be very frank software development for me started out as an experiment, that turned into a hobby that turned into a job, a job that I am good at, but I am inherently a problem solver. Angular, React, Nodejs, AWS, Azure etc. These are not what I do, these are only my tools. Just like a carpenter is not a sander, a planer.

## Purpose {class="align-left}

---

<div>
    <img class="img-fluid" src="static/img/light-bulb-03.gif" alt="light-bulb" />
</div>
