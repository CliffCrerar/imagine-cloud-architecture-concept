>  # Declaration:
>
>  ---
>
>  I, Clifford Crerar hereby swear that what is encapsulated in this document is from experience as a architect, technologist and dev. Together with knowledge gained by academic studies and a appetite to know and understand technology and practices associated with the field. I did not compose this document by googling what is a CTO, albeit google in the past has assisted me by means of other developers past experience to solve problems, like a lawyer would read case studies to build a case. 
>
>  How I keep my skills as sharp is:
>
>  - by reading articles from Medium, Hackernoon and Hasnode, 
>
>  - by answering questions on Quora, 
>
>  - by Reading and referring to technical documentation like on [Angular.io](www.angular.io), [Reactjs](https://reactjs.org/) and [RXJS](rxjs-dev.firebaseapp.com) to learn how to use new tools and functionalities about the framworks I use.
>
>  - by flexing my coding skills on hacker rank,
>
>  - and by learning about software engineering, interaction design and software project management
>
>  This is where my skills, knowledge and much of my experience comes from before I work my 8 to nth hour day as a software developer. I also hope this gives you a good idea about my work ethic and dedication to adding value to a company that values my participation. Give me a purpose and I will make you famous.

# Conclusions

---

What you can expect is diligence, ethics and integrity, dedication to learning new things and the use of the most modern technology. I will always strive to ensure I understand as well as be committed to ensuring the business gets and keeps the competative advantage and a healthy bottom line.

---

<div>
	<img class="img-fluid" src="static/img/coming-for-the-comp.gif" alt="coming for the competition">  
</div>

---

Site Author: ***[Clifford Crerar](www.cliff-crerar.tech)*** {style="text-align: center"}

---



# Technologies used in the production of this site:

---

