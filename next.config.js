const
  buildText = require('./static/buildText'),
  withPlugins = require('next-compose-plugins'),
  sourceMaps = require('@zeit/next-source-maps'),
  sass = require('@zeit/next-sass'),
  os = require('os'),
  mode = process.env.NODE_ENV === 'development',
  protocol = mode ? 'http' : 'https',
  host = mode ? 'localhost' : os.hostname(),
  port = mode ? ':3000' : '80' || '443',
  {exportMap} = require('./static/utils'),
  webpack = require('webpack'),
  dotenv = require('dotenv'),
  plugins = [
    [sass],
    [sourceMaps]
  ],
  fs=require('fs'),
  path=require('path');

//fs.writeFileSync(path.join(process.cwd(),'.env.build'),fs.readFileSync(process.cwd(),'.env'),'utf8');
dotenv.config();
buildText();
// fs.writeFileSync(path.join(process.cwd(),'.cookie'),Buffer(uuid().v4(),'base64'),'utf8');
console.log('|------------------------------------------|');
console.log('|--> MODE: ',mode?'Development':'Production');
console.log('|------------------------------------------|');
process.env.NODE_PATH = ['./', './static/'].join(os.platform() === 'win32' ? ';' : ':');
module.exports = withPlugins(plugins, {
  target: 'serverless',
  webpack: config => {
    const env = new webpack.EnvironmentPlugin({
      DEBUG: mode,
      ORIGIN_URL: `${protocol}://${host}${port}`,
      COOKIEID: process.env.COOKIEID
      // SECRETSTRING: process.env.SECRETSTRING
    })
    config.node = { fs: 'empty' };
    config.plugins.push(env)
    return config;
  }
})
