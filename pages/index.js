/**
 * Landing page
 */
import RotateCss from '../components/rotate-box';
import { string } from 'prop-types'
import { Component } from 'react';
// import SignInSvg from '../components/sign-in-icon';
import Head from 'next/head';
import Authentication from 'components/authentication';

class ManifestoTitle extends Component {
    constructor() {
        super()
        this.state = { st: false };
    }
    render() {
        return (
            <div onMouseOver={this.mouseEnterLeave}
                onMouseOut={this.mouseEnterLeave}
                className={`img-logo`}>
                <h1>Manifesto</h1>
                <p>Business and Tech</p>
            </div>
        )
    }
}

const title = 'Manifesto';
const defaultDescription = ''
const defaultOGURL = process.env.ORIGIN;
const defaultOGImage = ''


const AppHeadForThisPage = (props) => {
    return (
        <Head>
            <meta charSet="UTF-8" />
            <title>{props.pageTitle}</title>
            <meta name="description" content={props.description || defaultDescription}/>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" sizes="192x192" href="/static/touch-icon.png" />
            <link rel="apple-touch-icon" href="/static/touch-icon.png" />
            <link rel="mask-icon" href="/static/favicon-mask.svg" color="#49B882" />
            <link rel="icon" href="/static/favicon.ico" />
            <meta property="og:url" content={props.url || defaultOGURL} />
            <meta property="og:title" content={props.title || ''} />
            <meta property="og:description" content={props.description || defaultDescription}/>
            <meta name="twitter:site" content={props.url || defaultOGURL} />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:image" content={props.ogImage || defaultOGImage} />
            <meta property="og:image" content={props.ogImage || defaultOGImage} />
            <meta property="og:image:width" content="1200" />
            <meta property="og:image:height" content="630" />
            <link rel="stylesheet" href="static/css/index.css" />
            <link rel="stylesheet" href="static/css/animate.min.css" />
            {/* <link rel="stylesheet" href="static/css/bs.css" /> */}
        </Head>
    )
}

Head.propTypes = {
    title: string,
    description: string,
    url: string,
    ogImage: string
}

export default () => {
    const appTitle = 'Manifesto'
    return (
        <React.Fragment>
            <AppHeadForThisPage pageTitle={appTitle} />
            <main>
                <ManifestoTitle />
                <Authentication />
                <div className="btn-div">
                    {/* <SignInSvg opacity={1} /> */}
                </div>
                <RotateCss />
            </main>
        </React.Fragment>
    )
}