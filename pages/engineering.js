/**
 * Software engineering page
 */
import ReactMarkDown from '../static/md';
export default props => <ReactMarkDown>{props.pageText['p4-s1']}</ReactMarkDown>

