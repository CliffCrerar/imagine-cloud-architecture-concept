/**
 * Pages parent container
 */
import React from 'react'
import App from 'next/app'
import Layout from 'components/layout'
import cookies from 'js-cookie'
import checkCookies from 'route'
import { withRouter } from 'next/router'
class MyApp extends App {
  pageText = require('static/pageText.json')

  componentDidMount() {
    // console.log('APP DID MOUNT');
    this.checkCookies();
    this.paragraphStart();
  }
  componentDidUpdate = () => this.checkCookies();
  checkCookies() {
    // this.props.router.route
    //console.log('this.props.router.route: ', this.props.router);
    let httpCall;
    if (this.props.router.route === '/' || this.props.router.route === '/logged-out') {
      // console.log('do Nothing');
      return
    } else {
      // console.log('CHECK COOKIE AND AUTH');
      // console.log(cookies.get(process.env.COOKIEID));
      if(cookies.get(process.env.COOKIEID) === undefined){
        this.props.router.push('/logged-out');
      } else {
        httpCall = fetch('/api?action=getUser');
        // console.log('httpCall: ', httpCall);
        httpCall.then(httpRes=>{
          // console.log(httpRes);
          if (httpRes.status != 200) {
            this.props.router.push('/logged-out');
          }
        })
        httpCall.catch(err=>console.error('ERROR at _app checkcookies: ',err))
      }
    }
  }
  paragraphStart() {
    if (process.browser) {
      const paragraphs = document.getElementsByClassName('caps-start')
      for (let i = 0; i < paragraphs.length; i++) {
        let e = paragraphs[i];
        let text = e.innerHTML;
        let firstLetter = text.substring(0, 1);
        e.innerHTML = `<span class="big-Start">${firstLetter}</span>${text.substring(1, text.length).trim()}`
      }
    }
  }
  render() {
    const { Component, pageProps } = this.props
    return (
      <Layout>
        <Component pageText={this.pageText} {...pageProps} />
      </Layout>)
  }
}

export default withRouter(MyApp);