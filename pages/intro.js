/**
 * Intro page
 */
import ReactMarkDown from '../static/md';
export default (props) => {
    const { pageText } = props;
    return (
        <>
            <ReactMarkDown>{pageText['p1-s0']}</ReactMarkDown>
            <div className="">
                <hr />
                <img className="img-fluid" src="static/img/work_work_work.gif" alt="work-work-work" />
                <hr />
            </div>
            <ReactMarkDown>{pageText['p1-s1']}</ReactMarkDown>
        </>
    )
}