/**
 * AWS Media page
 */
import ReactMarkDown from '../static/md';
export default (props) => <ReactMarkDown>{props.pageText['p6-s1']}</ReactMarkDown>