/**
 * The tech setup page
 */
import ReactMarkDown from 'static/md.js'
export default (props) => <ReactMarkDown>{props.pageText['p2-s1']}</ReactMarkDown>