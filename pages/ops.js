/**
 * Operations page
 */
import ReactMarkDown from '../static/md';
import IaaS from 'components/iaas';
export default (props) => {
  const { pageText } = props;
  return (
    <>
      <ReactMarkDown>{pageText['p3-s1']}</ReactMarkDown>
      <IaaS />
      <ReactMarkDown>{pageText['p3-s2']}</ReactMarkDown>
    </>
  )
};