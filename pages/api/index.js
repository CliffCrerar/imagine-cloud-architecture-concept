import { type } from 'os';

// import Authentication from '../../components/authentication';
const
    dotenv = require('dotenv'),
    str = require('./decode'),
    cors = require('./_cors.js'),
    fetch = require('isomorphic-unfetch'),
    devMode = process.env.NODE_ENV === 'development';

devMode && dotenv.config();
// API Entry
function EndPoint(req, res) {
    try { delegate(req, res) }
    catch (err) { res.status(500).json({ caught: 'EndPoint', err }) };
}
// used to determine function to run according to query string action
function f() { return { login, getUser, logoutAll } }

async function delegate({ query, body, headers, cookies }, res) {
    const action = query.action;
    // console.log('action: ', action);
    const params = getParams({ query, body, headers, cookies }, action);
    // console.log('params: ', params);
    try {
        const
            dbRes = await f()[action](params).catch(err => { throw new Error(err) }),
            status = dbRes.status,
            statusText = dbRes.statusText;
        // console.log('dbRes: ', dbRes);
        if (action === 'logoutAll') {
            res.status(status).send(dbRes);
        } else {
            const respBody = await dbRes.json();
            res.status(status).json({ statusText, data: respBody.token });
        }
    } catch (err) {
        console.error('ERROR at delegate: ', err);
        res.status(500).json({ caught: 'Delegate', err })
    };
}

function getParams(callItems, action) {
    // console.log('GET PARAMS');
    // console.log('action: ', action);
    // console.log('callItems: ', callItems);
    switch (action) {
        case 'login': return JSON.parse(callItems.body).password;
        case 'getUser': return { headers: callItems.headers, cookies: callItems.cookies };
        case 'logoutAll': return { cookies: callItems.cookies };
    }
}

async function getUser({ cookies }) {
    try {
        const
            method = "GET",
            apiUrl = process.env.DB_URL.concat('users/me'),
            Authorization = 'Bearer ' + cookies.siteAuthToken,
            reqHeader = { Authorization, 'Content-Type': 'application/json' },
            res = await fetch(apiUrl, { method, headers: reqHeader });
        return res;
    } catch (err) {
        console.error('ERROR: ', err);
        return err;
    }
}

async function login(pw) {
    // console.log('pw: ', pw);
    // console.log('str.decode(pw): ', str.decode(pw));
    try {
        const
            method = 'POST',
            email = process.env.EMAIL,
            password = str.decode(pw),
            apiUrl = process.env.DB_URL.concat('users/login'),
            headers = { 'Content-Type': 'application/json' },
            body = JSON.stringify({ email, password }),
            callOpts = { method, headers, body },
            dbRes = await fetch(apiUrl, callOpts).catch((err) => console.log(err));

        // console.log('body: ', typeof body);
        // console.log('callOpts: ', callOpts);
        // console.log('apiUrl: ', apiUrl);
        // console.log('callOpts: ', callOpts);
        return dbRes;
    } catch (err) {
        console.error('ERROR Login: ', err)
        return err
    }
}

async function logoutAll({ cookies }) {
    // console.log('LG cookies: ', cookies);
    const
        method = 'POST',
        apiUrl = process.env.DB_URL.concat('users/me/logoutall'),
        Authorization = 'Bearer ' + cookies.siteAuthToken,
        headers = { Authorization },
        callOpts = { method, headers },
        res = await fetch(apiUrl, callOpts);
    // console.log('callOpts: ', callOpts);
    // console.log('res: ', res);
    return res;
}

export default cors(EndPoint);