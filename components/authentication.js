import { Component } from 'react';
import cookies from 'js-cookie';
import SignInButton from './sign-in-icon'
import Loader from './loader';

export default class Authentication extends Component {
    constructor() {
        super()
        this.handleSubmit = this.handleSubmit.bind(this);
        this.timeout = '1000'
        this.state = {
            loginForm: {
                position: 'relative',
                display: 'none'
            },
            authMsg: 'test',
            divStyle: {
                display: 'initial',
                maxWidth: '200px'
            },
            inputStyle: {
                width: '90%',
                position: 'relative'
            },
            buttonStyle: {
                padding: '10px 0',
                width: '100%',
                margin: '0',
                background: '#283593',
                color: 'white',
                fontSize: '1em',
                borderRadius: '3px',
                border: '0px'
            },
            signInButton: {
                display: 'none'
            },
            loading: {
                display: 'block'
            },
            msgAnimate: 'pulse iterate',
            pw: 'hide',
            ad: 'hide',
            auth: 'hide'
        }
    }
    componentDidMount() {
        // document.addEventListener('submit', this.handleSubmit);
        setTimeout(() => this.checkForToken(), this.timeout);
        (process.env.NODE_ENV === 'development') &&
            (document.getElementById('passwordInput').value = process.env.DEV_PW);
    }

    handleSubmit = async (ev) => {
        await ev.persist();
        await ev.preventDefault();
        // console.log('ev: ', ev.target[0]);
        // console.log('ev: ', ev.currentTarget[0].value);
        // console.log('ev: ', ev.currentTarget['passwordInput'].value);
        // console.log('ev: ', ev);
        // console.log('ev: ', ev.srcElement[0]);
        // await ev.persist();

        await this.showMessage('pw');
        const
            password = await btoa(ev.target['passwordInput'].value),
            method = "post",
            body = JSON.stringify({ password }),
            header = { 'Content-Type': 'application/json; charset=utf-8' },
            actionCallOpts = { body, header, method },
            res = await fetch('/api?action=login', actionCallOpts)
                .then(res => res.json())
                .catch(err => console.error('FETCH ERROR: ', err));
        // console.log('res: ', res);
        // console.log('res: ', res.body.blob());   
        if (res.statusText != 'OK') {
            this.setState({ authMsg: 'Access Denied!' })
            this.showMessage('ad');
        } else {
            cookies.set('siteAuthToken', res.data);
            this.showMessage('auth');
            setTimeout(() => {
                this.setState({ signInButton: { display: 'block' }, loginForm: { display: 'none' } });
            }, 2000)

        }
        /*console.log('res: ', res);*/
    }
    checkForToken() {
        const token = cookies.get('siteAuthToken');
        if (token === undefined || token === 'undefined') {
            console.log('no token');
            this.showLoginForm('no-token');
        } else {
            console.log('token');
            this.getUser()
                // unauthorised
                .then(({ res, body }) => this.tokenValidation(res, body))
                .catch(err => console.error(err));
        }
    }
    getUser = async () => {
        const
            header = new Headers().append('Content-Type', 'application/json'),
            method = 'GET',
            res = await fetch('/api?action=getUser', { method, header }),
            body = await res.json();
        return ({ res, body });
    }
    showMessage(msg) {
        ['pw', 'auth', 'ad'].forEach(e => this.setState({ [e]: 'hide' }));
        this.setState({ [msg]: 'show' });
    }
    showLoginForm = () => this.setState({ loginForm: { display: 'block', }, loading: { display: 'none', } });
    tokenValidation = (res, body) => {
        console.log('Token Validation');
        if (res.ok) {
            this.setState({ signInButton: { display: 'block' }, loading: { display: 'none' } })
        } else {
            //console.log('body: ', body);
            this.showLoginForm();
        }
    }

    render() {
        return (
            <div
                // onKeyDown={key => this.onKePressEnter(key)}
                className="animated slideInRight phrase-div"
                style={this.state.divStyle}>
                <div style={this.state.loginForm}>
                    <form id="pass" action="/api" onSubmit={this.handleSubmit}>
                        <div style={{ position: 'relative', display: 'block' }}>
                            <input
                                id="passwordInput"
                                style={this.state.inputStyle}
                                type="password"
                                autoFocus
                                name="passwordInput"
                                placeholder="password input"
                                required />
                        </div>
                        <div className="text-center">
                            <button
                                type="submit"
                                style={this.state.buttonStyle}
                                className="auth-button">Authenticate</button>
                        </div>
                    </form>
                    <div className="text-center auth-msg-font" style={{ width: '200px' }}>
                        <div className={`${this.state.pw} animated pulse iterate`}>Please wait!</div>
                        <div className={`${this.state.auth}`}>Authenticated !</div>
                        <div className={`${this.state.ad} access-denied`}>
                            <h4>Access Denied</h4>
                            <small className="access-denied-msg">Please input the correct password or contact <a href="mailto:support@infinityarc.net">support@infinityarc.net</a> for further assistance.</small>
                        </div>
                    </div>
                </div>

                <div id="sign-on-button" style={this.state.signInButton}>
                    <SignInButton />
                    <div>Click here</div>
                </div>
                <div className="loader" style={this.state.loading}>
                    <Loader
                        height="auto"
                        width="50px"
                        position="relative" />
                    <div className="animated pulse iterate">Please wait</div>
                </div>
                <style jsx>{`
                    .button-style{
                            padding: 10px 0;
                            width: 100%;
                            margin: 0;
                            background: '#283593';
                            color: white;
                            font-size: 1em;
                            border-radius: 3px;
                            border: 0px solid transparent;
                    }          
                    .access-denied-msg{
                        color: black;
                        background: white;
                        border-radius: 4px;
                        padding: 10px;
                        display: block;
                        word-wrap: break-word;
                        overflow: wrap;
                    }
                `}</style>
            </div>
        )
    }
}
