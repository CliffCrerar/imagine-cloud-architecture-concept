/**
 * component was replaced with layout
 * DEPRECATED: Navigation control and master layout
 */
import React, { Component } from 'react'
import { LeftArrow, RightArrow } from 'left-right-icons';
import { withRouter } from 'next/router'
import Link from 'next/link'
import AppHead from 'head';

const
  documentTitle = "Re-Imagining Technology Application 🤔 ",
  subTitle = "Miles ahead of the competition",
  links = [
    { href: 'intro', label: 'Intro', pageTitle: 'Introduction' },
    { href: 'set-up', label: 'The Setup',pageTitle: 'Introduction' },
    { href: 'ops', label: 'Operations',pageTitle: 'Introduction' },
    { href: 'engineering', label: 'Engineering',pageTitle: 'Introduction' },
    { href: 'devops', label: 'DevOps',pageTitle: 'Introduction' },
    { href: 'info-media', label: 'Cloud Info Media',pageTitle: 'Introduction' },
    { href: 'declaration', label: 'Declaration',pageTitle: 'Introduction' }
  ].map(link => {
    link.key = `nav-link-${link.href}-${link.label}`
    return link
  });
// Nav arrow buttons
class NavArrows extends Component {
  constructor(props) {
    super(props)
    this.state = { state: 'none', hideLeftArrow: false, hideRightArrow: false, }
    this.thisPageNo = links.map(l => l.href).indexOf(this.props.router.route.replace('/', ''));
  };
  // Component mounts
  componentDidMount = () => this.handleControlsDisplay();
  // Handle control display
  handleControlsDisplay = () => {
    if (this.thisPageNo === 0) {
      return this.setState({ hideLeftArrow: true });
    } else if (this.thisPageNo === (links.length - 1)) { return this.setState({ hideRightArrow: true }); }
  };
  // Handle click
  handleClick(e) {
    const newLink = e === 'left' ? this.thisPageNo - 1 : this.thisPageNo + 1;
    Promise.resolve(
      this.props.router.push('/' + links[newLink].href)
    ).then(() => {
      window.scrollTo(top)
    });
  };

  // Renders
  render() {
    // component jsx
    return (
      <div className="page-change-arrows">
        <div
          hidden={true}
          className={`left-arrow ${(this.state.hideLeftArrow && 'hide-control')}`}
          onClick={e => this.handleClick('left')}>
          <LeftArrow />
          <div className="text">Prev Page</div>
        </div>
        <div
          className={((this.state.hideLeftArrow || this.state.hideRightArrow) ? 'show-control' : 'hide-control')}>

        </div>
        <div
          hidden={this.state.hideRightArrow}
          className={`right-arrow ${(this.state.hideRightArrow && 'hide-control')}`}
          onClick={e => this.handleClick('right')}>
          <div className="text">Next Page</div>
          <RightArrow />
        </div>
      </div>
    )
  };
}

// Nav links display at top and bottom of page
const DisplayLinks = (props) => {
  return links.map(({ key, href, label }) => {
    function isActive(route) {
      if ('/' + href === route) {
        return 'isActiveNow';
      } else { return 'not-active'; }
    }
    return (
      <li key={key} className={isActive(props.router.route)}>
        <Link href={'/' + href}><a style={{ textDecoration: 'none' }}>{label}</a></Link>
      </li>)
  })
}

class MousePos extends Component {
  constructor() {
    super()
    this.state = { x: 0, y: 0 }
  }
  classes = {
    style1: {
      position: "fixed",
      padding: '10px',
      top: '10px',
      right: '10px',
      background: '#7bc97f',
      borderRadius: '4px',
    },
    style2: {
      color: 'white'
    }
  }
  componentDidMount = () => document.addEventListener('mousemove', this.handleMouseMove)
  handleMouseMove = (e) => this.setState({ x: e.clientX, y: e.clientY })
  render() {
    return (
      <div style={this.classes.style1}>
        <div style={this.classes.style2}>Mouse X: {this.state.x}</div>
        <div style={this.classes.style2}>Mouse Y: {this.state.y}</div>
      </div>
    )
  }
}


const SiteContainer = (props) => {
  return (
    <React.Fragment>
      {/* Navigation bar */}
      <AppHead title={links.filter(link=>link.href===props.router.route)[0].pageTitle}/>
      <nav className="nav-links">
        <div className="navbar-left">
          <h4>NAV</h4>
          <ul><DisplayLinks router={props.router} /></ul>
          <span>|</span>
        </div>
        <small>Page by <a style={{ textDecoration: 'none' }}>Cliff Crerar</a></small>
      </nav>
      {/* PAGE HEADER */}
      <header>
        <h1 className="title">{documentTitle}</h1>
        <div>
          <small>{subTitle}</small>
        </div>
        <hr className="header-hr" />
      </header>
      {/* PAGE BODY */}
      <main>
        {props.children}
        <hr />
        <ul className="nav-links"><DisplayLinks router={props.router} /></ul>
        <hr />
        <NavArrows router={props.router} /> {/* bottom navigation */}
      </main>
      <footer>
        {/* FOOTER */}
      </footer>
      {/* <MousePos /> */}
    </React.Fragment>
  )
}

export default withRouter(SiteContainer)
