/**
 * COMPONENT: Main layout
 */

/**
* Navigation control and master layout
*/
import React, { Component } from 'react'
import NavArrows from './page-nav-arrows'
import { withRouter } from 'next/router'
import Links from 'static/utils/links'
import Link from 'next/link'
import AppHead from 'head';

const
    documentTitle = "Re-Imagining Technology Application 🤔",
    subTitle = "Miles ahead of the competition",
    links = Links.main.map(link => {
        link.key = `nav-link-${link.href}-${link.label}`
        return link
    });

// Nav links display at top and bottom of page
const DisplayLinks = (props) => {
    return links.map(({ key, href, label, pageTitle }, i) => {
        // console.log('i: ', i);
        function isActive(route) {
            if ('/' + href === route) {
                return 'isActiveNow';
            } else { return 'not-active'; }
        }
        if (i === 0) {
            return (
                <li key={key} className={isActive(props.router.route)}>
                    <Link href={'/' + href} as={'/' + href}>
                        <a title={'Manifesto'} style={{ textDecoration: 'none' }}>{label}</a>
                    </Link>
                </li>
            )
        } else {
            return (
                <li key={key} className={isActive(props.router.route)}>
                    <Link href={'/' + href} as={'/' + href}>
                        <a title={pageTitle} style={{ textDecoration: 'none' }}>{label}</a>
                    </Link>
                </li>
            )
        }
    })
}

class MousePos extends Component {
    constructor() {
        super()
        this.state = { x: 0, y: 0 }
    }
    classes = {
        style1: {
            position: "fixed",
            padding: '10px',
            top: '10px',
            right: '10px',
            background: '#7bc97f',
            borderRadius: '4px',
        },
        style2: {
            color: 'white'
        }
    }
    componentDidMount = () => document.addEventListener('mousemove', this.handleMouseMove)
    handleMouseMove = (e) => this.setState({ x: e.clientX, y: e.clientY })
    render() {
        return (
            <div style={this.classes.style1}>
                <div style={this.classes.style2}>Mouse X: {this.state.x}</div>
                <div style={this.classes.style2}>Mouse Y: {this.state.y}</div>
            </div>
        )
    }
}

function pageTitle(props) {
    let title;
    try {
        const uLinks = Links.main.concat(Links.nonNavLinks);
        // console.log('uLinks: ', uLinks);
        return uLinks.filter(link => '/'.concat(link.href) === props.router.pathname)[0].pageTitle;
    } catch (err) {
        return 'Manifesto'
    }
}

const PagesLayout = (props) => (
    <React.Fragment>
        <AppHead title={pageTitle(props)} />
        {/* Navigation bar */}

        <nav className="nav-links">
            <div className="navbar-left">
                <h4>NAV</h4>
                <ul><DisplayLinks router={props.router} /></ul>
            </div>
            <div className="logout-link">
                <a href="/logged-out">Logout</a>
                <small>Page by <a style={{ textDecoration: 'none' }}>Cliff Crerar</a></small>
            </div>
        </nav>
        <hr className="nav-bottom-line" />
        {/* PAGE HEADER */}
        <header>
            <h1 className="title">{documentTitle}</h1>
            <div>
                <small>{subTitle}</small>
            </div>
            <hr className="header-hr" />
        </header>
        {/* PAGE BODY */}
        <main>
            {props.children}
            <hr />
            <ul className="nav-links"><DisplayLinks router={props.router} /></ul>
            <hr />
            <NavArrows router={props.router} links={Links.main} /> {/* bottom navigation */}
        </main>
        <footer>
            {/* FOOTER */}
        </footer>
        {/* <MousePos /> */}
    </React.Fragment>
)


const SiteContainer = (props) => {
    props.router.events.on('routeChangeStart', url => {
        console.log('ROUTE START: ', url);
    })
    if (['/', '', '/logged-out'].includes(props.router.route)) {
        return props.children;
    } else {
        return <PagesLayout router={props.router} children={props.children} />
    }
}
export default withRouter(SiteContainer)
