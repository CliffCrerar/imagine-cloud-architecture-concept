# Imagine Insurance CTO Tender

Online manifesto authored to tender for the position of CTO for imagine insurance.

Intended Audience: *Josh Kaplan*

---

Project Notes.

- In the config file, the function  `buildText`  converts  the files in `static/md` into plain text, then converts it to a `JSON` file named `pageText.json` that is read by `_app` that creates the content for the pages.

- The file `md.js` is what the application uses to read and display the content from the file `pageText.json`.

In the components folder all files starting with _ are files that have become redundant.

---
> GIT Link: `https://gitlab.com/CliffCrerar/imagine-cloud-architecture-concept.git`
---

> Tech used:
- `Next.js`
- `Gitlab`
- `Sass`
- `Markdown`
- `Reactjs`
- `JSON`
- `Zeit Now` (deployment)